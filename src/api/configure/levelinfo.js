import request from "@/router/axios";

export const getList = (current, size, params) => {
  return request({
    url: "/api/fine-configure/levelinfo/list",
    method: "get",
    params: {
      ...params,
      current,
      size,
    },
  });
};

export const getDetail = (id) => {
  return request({
    url: "/api/fine-configure/levelinfo/detail",
    method: "get",
    params: {
      id,
    },
  });
};

export const remove = (ids) => {
  return request({
    url: "/api/fine-configure/levelinfo/remove",
    method: "post",
    params: {
      ids,
    },
  });
};

export const add = (row) => {
  return request({
    url: "/api/fine-configure/levelinfo/submit",
    method: "post",
    data: row,
  });
};

export const update = (row) => {
  return request({
    url: "/api/fine-configure/levelinfo/submit",
    method: "post",
    data: row,
  });
};
