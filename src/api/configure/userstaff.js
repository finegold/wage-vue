import request from "@/router/axios";

export const getList = (current, size, params) => {
  return request({
    url: "/api/fine-configure/userstaff/list",
    method: "get",
    params: {
      ...params,
      current,
      size,
    },
  });
};

export const getPostList = () => {
  return request({
    url: "/api/fine-system/post/list4dict",
    method: "get",
  });
};

export const getDutyList = () => {
  return request({
    url: "/api/fine-configure/duties/list4dict",
    method: "get",
  });
};

export const getDetail = (id) => {
  return request({
    url: "/api/fine-configure/userstaff/detail",
    method: "get",
    params: {
      id,
    },
  });
};

export const remove = (ids) => {
  return request({
    url: "/api/fine-configure/userstaff/remove",
    method: "post",
    params: {
      ids,
    },
  });
};

export const progressive = (ids) => {
  return request({
    url: "/api/fine-configure/userstaff/progressive",
    method: "post",
    params: {
      ids,
    },
  });
};

export const add = (row) => {
  return request({
    url: "/api/fine-configure/userstaff/submit",
    method: "post",
    data: row,
  });
};

export const update = (row) => {
  return request({
    url: "/api/fine-configure/userstaff/submit",
    method: "post",
    data: row,
  });
};
