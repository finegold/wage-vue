import request from "@/router/axios";
//等级工资统计
export const getLevelList = (current, size, params) => {
  return request({
    url: "/api/blade-wages/wages/level-statistics",
    method: "get",
    params: {
      ...params,
    },
  });
};
//按人员统计
export const getPersonList = (current, size, params) => {
  return request({
    url: "/api/blade-wages/wages/person-statistics",
    method: "get",
    params: {
      ...params,
    },
  });
};
//按人员统计
export const getDeptLazyList = () => {
  return request({
    url: "/api/fine-system/dept/lazy-list",
    method: "get",
  });
};
//员工统计
export const getEmployeeList = (current, size, params) => {
  return request({
    url: "/api/blade-wages/wages/employee-statistics",
    method: "get",
    params: {
      ...params,
    },
  });
};

export const getDictionary = (code = "wages_type") => {
  return request({
    url: "/api/fine-system/dict/dictionary",
    method: "get",
    params: {
      code,
    },
  });
};
