import request from "@/router/axios";

export const getList = (current, size, params) => {
  return request({
    url: "/api/blade-wages/socialhousing/list",
    method: "get",
    params: {
      ...params,
      current,
      size,
    },
  });
};

export const getDetail = (id) => {
  return request({
    url: "/api/blade-wages/socialhousing/detail",
    method: "get",
    params: {
      id,
    },
  });
};

export const remove = (ids) => {
  return request({
    url: "/api/blade-wages/socialhousing/remove",
    method: "post",
    params: {
      ids,
    },
  });
};

export const add = (row) => {
  return request({
    url: "/api/blade-wages/socialhousing/submit",
    method: "post",
    data: row,
  });
};

export const update = (row) => {
  return request({
    url: "/api/blade-wages/socialhousing/submit",
    method: "post",
    data: row,
  });
};

export const settlement = () => {
  return request({
    url: "/api/blade-wages/socialhousing/settlement",
    method: "post",
  });
};

export const nosettlement = (accountPeriod) => {
  return request({
    url: "/api/blade-wages/socialhousing/noSettlement",
    method: "post",
    params: {
      accountPeriod,
    },
  });
};

export const importWages = (file, accountPeriod) => {
  const formData = new FormData();
  formData.append("file", file);
  formData.append("accountPeriod", accountPeriod);
  return request({
    url: "/api/blade-wages/socialhousing/import-socialHousing",
    method: "post",
    headers: {
      "Content-Type": "multipart/form-data",
    },
    data: formData,
  });
};
