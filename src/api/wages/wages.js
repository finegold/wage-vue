import request from "@/router/axios";

export const getList = (current, size, params) => {
  return request({
    url: "/api/blade-wages/wages/list",
    method: "get",
    params: {
      ...params,
    },
  });
};

export const getPage = (current, size, params) => {
  return request({
    url: "/api/blade-wages/wages/page",
    method: "get",
    params: {
      ...params,
    },
  });
};

export const getExportParam = () => {
  return request({
    url: "/api/blade-wages/wages/export-param",
    method: "get",
  });
};

export const exportTemplate = (fields) => {
  return request({
    url: "/api/blade-wages/wages/export-template",
    method: "get",
    params: {
      fields,
    },
  });
};

export const getDetail = (id) => {
  return request({
    url: "/api/blade-wages/wages/detail",
    method: "get",
    params: {
      id,
    },
  });
};

export const remove = (ids) => {
  return request({
    url: "/api/blade-wages/wages/remove",
    method: "post",
    params: {
      ids,
    },
  });
};

export const add = (row) => {
  return request({
    url: "/api/blade-wages/wages/submit",
    method: "post",
    data: row,
  });
};

export const update = (row) => {
  return request({
    url: "/api/blade-wages/wages/submit",
    method: "post",
    data: row,
  });
};

export const settlement = () => {
  return request({
    url: "/api/blade-wages/wages/settlement",
    method: "post",
  });
};

export const nosettlement = (accountPeriod) => {
  return request({
    url: "/api/blade-wages/wages/noSettlement",
    method: "post",
    params: {
      accountPeriod,
    },
  });
};

export const batchAdd = (accountPeriod) => {
  return request({
    url: "/api/blade-wages/wages/batchAdd",
    method: "post",
    params: {
      accountPeriod,
    },
  });
};

export const importWages = (file, accountPeriod) => {
  const formData = new FormData();
  formData.append("file", file);
  formData.append("accountPeriod", accountPeriod);
  return request({
    url: "/api/blade-wages/wages/import-wages",
    method: "post",
    headers: {
      "Content-Type": "multipart/form-data",
    },
    data: formData,
  });
};

export const importWageNon = (file, accountPeriod) => {
  const formData = new FormData();
  formData.append("file", file);
  formData.append("accountPeriod", accountPeriod);
  return request({
    url: "/api/blade-wages/wages/import-wages-non",
    method: "post",
    headers: {
      "Content-Type": "multipart/form-data",
    },
    data: formData,
  });
};
